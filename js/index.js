$(function(){
	$("[data-toggle='tooltip']").tooltip();
	$('.carousel').carousel({
		interval: 1000
	});
	$('#contacto').on('show.bs.modal', function (e){
		console.log('el model contacto se está mostrando');
		$('#contactoBtn').removeClass('btn-success');
		$('#contactoBtn').addClass('btn-primary');
		$('#contactoBtn').prop('disabled', true);
	});
	$('#contacto').on('shown.bs.modal', function (e){
		console.log('el model contacto se mostró');
	});
	$('#contacto').on('hide.bs.modal', function (e){
		console.log('el model contacto se oculta');
	});
	$('#contacto').on('hidden.bs.modal', function (e){
		console.log('el model contacto se ocultó');
		$('#contactoBtn').prop('disabled', false);
	})
});
